#ifndef I_MODULE_OWNER_HPP
#define I_MODULE_OWNER_HPP

/**
 * @file IModuleOwner.hpp
 *
 * This module declares the IModuleOwner interface.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"
#include "Modules.hpp"

#include <expat.h>
#include <string>

/**
 * This is the interface to an object which owns a module.
 */
class IModuleOwner {
public:
    virtual ~IModuleOwner() {}

    /**
     * @todo Needs documentation
     */
    virtual void BeginLoadingModule(Module* module, int id, const XML_Char** atts) = 0;

    /**
     * @todo Needs documentation
     */
    virtual void StartModuleElement(Module* module, const XML_Char* name, const XML_Char** atts) = 0;

    /**
     * @todo Needs documentation
     */
    virtual void ModuleElementCharacterData(Module* module, const XML_Char* s, int len) = 0;

    /**
     * @todo Needs documentation
     */
    virtual void EndModuleElement(Module* module, const XML_Char* name) = 0;

    /**
     * @todo Needs documentation
     */
    virtual void EndLoadingModules(const Modules::SerializedModules& serializedModules) = 0;
};

#endif /* I_MODULE_OWNER_HPP */
