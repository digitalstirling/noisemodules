/**
 * @file VoronoiModule.cpp
 *
 * This module contains the implementation of the VoronoiModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "VoronoiModule.hpp"

#include <algorithm>
#include <noise.h>

VoronoiModule::VoronoiModule()
    : Module("Voronoi", new noise::module::Voronoi())
{
    noise::module::Voronoi* voronoi = static_cast< noise::module::Voronoi* >(_module);

    // Seed
    Parameter enableDistance = { "Enable Distance", ParameterTypeBoolean };
    enableDistance.value.boolean = voronoi->IsDistanceEnabled();
    _parameters.push_back(enableDistance);

    // Frequency
    Parameter frequency = { "Frequency", ParameterTypeDecimal };
    frequency.value.decimal = voronoi->GetFrequency();
    _parameters.push_back(frequency);

    // Lacunarity
    Parameter displacement = { "Displacement", ParameterTypeDecimal };
    displacement.value.decimal = voronoi->GetDisplacement();
    _parameters.push_back(displacement);

    // Quality
    Parameter seed = { "Seed", ParameterTypeInteger };
    seed.value.integer = voronoi->GetSeed();
    _parameters.push_back(seed);
}

VoronoiModule::~VoronoiModule() {
}

void VoronoiModule::UpdateParameters() {
    // Put parameters within bounds to avoid crash.

    // Apply parameters to module.
    noise::module::Voronoi* voronoi = static_cast< noise::module::Voronoi* >(_module);
    voronoi->EnableDistance(_parameters[0].value.boolean);
    voronoi->SetFrequency(_parameters[1].value.decimal);
    voronoi->SetDisplacement(_parameters[2].value.decimal);
    voronoi->SetSeed(_parameters[3].value.integer);
}
