/**
 * @file SelectModule.cpp
 *
 * This module contains the implementation of the SelectModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "SelectModule.hpp"

#include <algorithm>
#include <noise.h>

SelectModule::SelectModule()
    : Module("Select", new noise::module::Select())
{
    noise::module::Select* select = static_cast< noise::module::Select* >(_module);

    // Lower Bound
    Parameter lowerBound = { "Lower Bound", ParameterTypeDecimal };
    lowerBound.value.decimal = select->GetLowerBound();
    _parameters.push_back(lowerBound);

    // Upper Bound
    Parameter upperBound = { "Upper Bound", ParameterTypeDecimal };
    upperBound.value.decimal = select->GetUpperBound();
    _parameters.push_back(upperBound);

    // Edge Falloff
    Parameter edgeFalloff = { "Edge Falloff", ParameterTypeDecimal };
    edgeFalloff.value.decimal = select->GetEdgeFalloff();
    _parameters.push_back(edgeFalloff);

}

SelectModule::~SelectModule() {
}

void SelectModule::UpdateParameters() {
    // Put parameters within bounds to avoid crash.

    // Apply parameters to module.
    noise::module::Select* select = static_cast< noise::module::Select* >(_module);
    select->SetBounds(_parameters[0].value.decimal, _parameters[1].value.decimal);
    select->SetEdgeFalloff(_parameters[2].value.decimal);
}
