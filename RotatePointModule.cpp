/**
 * @file RotatePointModule.cpp
 *
 * This module contains the implementation of the RotatePointModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "RotatePointModule.hpp"

#include <algorithm>
#include <noise.h>

RotatePointModule::RotatePointModule()
    : Module("RotatePoint", new noise::module::RotatePoint())
{
    noise::module::RotatePoint* rotatePoint = static_cast< noise::module::RotatePoint* >(_module);

    // X Angle
    Parameter xAngle = { "X Angle", ParameterTypeDecimal };
    xAngle.value.decimal = rotatePoint->GetXAngle();
    _parameters.push_back(xAngle);

    // Y Angle
    Parameter yAngle = { "Y Angle", ParameterTypeDecimal };
    yAngle.value.decimal = rotatePoint->GetYAngle();
    _parameters.push_back(yAngle);

    // Z Angle
    Parameter zAngle = { "Z Angle", ParameterTypeDecimal };
    zAngle.value.decimal = rotatePoint->GetZAngle();
    _parameters.push_back(zAngle);
}

RotatePointModule::~RotatePointModule() {
}

void RotatePointModule::UpdateParameters() {
    // Put parameters within bounds to avoid crash.

    // Apply parameters to module.
    noise::module::RotatePoint* rotatePoint = static_cast< noise::module::RotatePoint* >(_module);
    rotatePoint->SetXAngle(_parameters[0].value.decimal);
    rotatePoint->SetYAngle(_parameters[1].value.decimal);
    rotatePoint->SetZAngle(_parameters[2].value.decimal);
}
