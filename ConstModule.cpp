/**
 * @file ConstModule.cpp
 *
 * This module contains the implementation of the ConstModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "ConstModule.hpp"

#include <algorithm>
#include <noise.h>

ConstModule::ConstModule()
    : Module("Const", new noise::module::Const())
{
    noise::module::Const* constModule = static_cast< noise::module::Const* >(_module);

    // Value
    Parameter value = { "Value", ParameterTypeDecimal };
    value.value.decimal = constModule->GetConstValue();
    _parameters.push_back(value);
}

ConstModule::~ConstModule() {
}

void ConstModule::UpdateParameters() {
    // Put parameters within bounds to avoid crash.

    // Apply parameters to module.
    noise::module::Const* constModule = static_cast< noise::module::Const* >(_module);
    constModule->SetConstValue(_parameters[0].value.decimal);
}
