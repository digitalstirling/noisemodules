#ifndef SCALE_POINT_MODULE_HPP
#define SCALE_POINT_MODULE_HPP

/**
 * @file ScalePointModule.hpp
 *
 * This module declares the ScalePointModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

/**
 * @todo Needs documentation
 */
class ScalePointModule
    : public Module
{
    // Public methods
public:
    /**
     * This is the instance constructor.
     */
    ScalePointModule();

    /**
     * This is the instance destructor.
     */
    ~ScalePointModule();

    // Module
protected:
    void UpdateParameters();
};

#endif /* SCALE_POINT_MODULE_HPP */
