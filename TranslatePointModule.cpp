/**
 * @file TranslatePointModule.cpp
 *
 * This module contains the implementation of the TranslatePointModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "TranslatePointModule.hpp"

#include <algorithm>
#include <noise.h>

TranslatePointModule::TranslatePointModule()
    : Module("TranslatePoint", new noise::module::TranslatePoint())
{
    noise::module::TranslatePoint* translatePoint = static_cast< noise::module::TranslatePoint* >(_module);

    // X Translation
    Parameter xTranslation = { "X Translation", ParameterTypeDecimal };
    xTranslation.value.decimal = translatePoint->GetXTranslation();
    _parameters.push_back(xTranslation);

    // Y Translation
    Parameter yTranslation = { "Y Translation", ParameterTypeDecimal };
    yTranslation.value.decimal = translatePoint->GetYTranslation();
    _parameters.push_back(yTranslation);

    // Z Translation
    Parameter zTranslation = { "Z Translation", ParameterTypeDecimal };
    zTranslation.value.decimal = translatePoint->GetZTranslation();
    _parameters.push_back(zTranslation);
}

TranslatePointModule::~TranslatePointModule() {
}

void TranslatePointModule::UpdateParameters() {
    // Put parameters within bounds to avoid crash.

    // Apply parameters to module.
    noise::module::TranslatePoint* translatePoint = static_cast< noise::module::TranslatePoint* >(_module);
    translatePoint->SetXTranslation(_parameters[0].value.decimal);
    translatePoint->SetYTranslation(_parameters[1].value.decimal);
    translatePoint->SetZTranslation(_parameters[2].value.decimal);
}
