#ifndef TURBULENCE_MODULE_HPP
#define TURBULENCE_MODULE_HPP

/**
 * @file TurbulenceModule.hpp
 *
 * This module declares the TurbulenceModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

/**
 * @todo Needs documentation
 */
class TurbulenceModule
    : public Module
{
    // Public methods
public:
    /**
     * This is the instance constructor.
     */
    TurbulenceModule();

    /**
     * This is the instance destructor.
     */
    ~TurbulenceModule();

    // Module
protected:
    void UpdateParameters();
};

#endif /* TURBULENCE_MODULE_HPP */
