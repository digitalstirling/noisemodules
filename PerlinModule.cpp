/**
 * @file PerlinModule.cpp
 *
 * This module contains the implementation of the PerlinModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "PerlinModule.hpp"

#include <algorithm>
#include <noise.h>

PerlinModule::PerlinModule()
    : Module("Perlin", new noise::module::Perlin())
{
    noise::module::Perlin* perlin = static_cast< noise::module::Perlin* >(_module);

    // Seed
    Parameter seed = { "Seed", ParameterTypeInteger };
    seed.value.integer = perlin->GetSeed();
    _parameters.push_back(seed);

    // Frequency
    Parameter frequency = { "Frequency", ParameterTypeDecimal };
    frequency.value.decimal = perlin->GetFrequency();
    _parameters.push_back(frequency);

    // Lacunarity
    Parameter lacunarity = { "Lacunarity", ParameterTypeDecimal };
    lacunarity.value.decimal = perlin->GetLacunarity();
    _parameters.push_back(lacunarity);

    // Quality
    Parameter quality = { "Quality", ParameterTypeQuality };
    quality.value.quality = perlin->GetNoiseQuality();
    _parameters.push_back(quality);

    // Octaves
    Parameter octaves = { "Octaves", ParameterTypeInteger };
    octaves.value.integer = perlin->GetOctaveCount();
    _parameters.push_back(octaves);

    // Persistence
    Parameter persistence = { "Persistence", ParameterTypeDecimal };
    persistence.value.decimal = perlin->GetPersistence();
    _parameters.push_back(persistence);
}

PerlinModule::~PerlinModule() {
}

void PerlinModule::UpdateParameters() {
    // Put parameters within bounds to avoid crash.
    _parameters[4].value.integer = std::max(1, std::min(noise::module::PERLIN_MAX_OCTAVE, _parameters[4].value.integer));

    // Apply parameters to module.
    noise::module::Perlin* perlin = static_cast< noise::module::Perlin* >(_module);
    perlin->SetSeed(_parameters[0].value.integer);
    perlin->SetFrequency(_parameters[1].value.decimal);
    perlin->SetLacunarity(_parameters[2].value.decimal);
    perlin->SetNoiseQuality(_parameters[3].value.quality);
    perlin->SetOctaveCount(_parameters[4].value.integer);
    perlin->SetPersistence(_parameters[5].value.decimal);
}
