/**
 * @file ExponentModule.cpp
 *
 * This module contains the implementation of the ExponentModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "ExponentModule.hpp"

#include <algorithm>
#include <noise.h>

ExponentModule::ExponentModule()
    : Module("Exponent", new noise::module::Exponent())
{
    noise::module::Exponent* exponent = static_cast< noise::module::Exponent* >(_module);

    // Exponent
    Parameter exponentValue = { "Exponent", ParameterTypeDecimal };
    exponentValue.value.decimal = exponent->GetExponent();
    _parameters.push_back(exponentValue);
}

ExponentModule::~ExponentModule() {
}

void ExponentModule::UpdateParameters() {
    // Put parameters within bounds to avoid crash.

    // Apply parameters to module.
    noise::module::Exponent* exponent = static_cast< noise::module::Exponent* >(_module);
    exponent->SetExponent(_parameters[0].value.decimal);
}
