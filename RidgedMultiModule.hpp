#ifndef RIDGED_MULTI_MODULE_HPP
#define RIDGED_MULTI_MODULE_HPP

/**
 * @file RidgedMultiModule.hpp
 *
 * This module declares the RidgedMultiModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

/**
 * @todo Needs documentation
 */
class RidgedMultiModule
    : public Module
{
    // Public methods
public:
    /**
     * This is the instance constructor.
     */
    RidgedMultiModule();

    /**
     * This is the instance destructor.
     */
    ~RidgedMultiModule();

    // Module
protected:
    void UpdateParameters();
};

#endif /* RIDGED_MULTI_MODULE_HPP */
