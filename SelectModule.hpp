#ifndef SELECT_MODULE_HPP
#define SELECT_MODULE_HPP

/**
 * @file SelectModule.hpp
 *
 * This module declares the SelectModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

/**
 * @todo Needs documentation
 */
class SelectModule
    : public Module
{
    // Public methods
public:
    /**
     * This is the instance constructor.
     */
    SelectModule();

    /**
     * This is the instance destructor.
     */
    ~SelectModule();

    // Module
protected:
    void UpdateParameters();
};

#endif /* SELECT_MODULE_HPP */
