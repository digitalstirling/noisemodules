#ifndef SCALE_BIAS_MODULE_HPP
#define SCALE_BIAS_MODULE_HPP

/**
 * @file ScaleBiasModule.hpp
 *
 * This module declares the ScaleBiasModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

/**
 * @todo Needs documentation
 */
class ScaleBiasModule
    : public Module
{
    // Public methods
public:
    /**
     * This is the instance constructor.
     */
    ScaleBiasModule();

    /**
     * This is the instance destructor.
     */
    ~ScaleBiasModule();

    // Module
protected:
    void UpdateParameters();
};

#endif /* SCALE_BIAS_MODULE_HPP */
