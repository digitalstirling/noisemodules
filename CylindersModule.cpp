/**
 * @file CylindersModule.cpp
 *
 * This module contains the implementation of the CylindersModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "CylindersModule.hpp"

#include <algorithm>
#include <noise.h>

CylindersModule::CylindersModule()
    : Module("Cylinders", new noise::module::Cylinders())
{
    noise::module::Cylinders* cylinders = static_cast< noise::module::Cylinders* >(_module);

    // Frequency
    Parameter frequency = { "Frequency", ParameterTypeDecimal };
    frequency.value.decimal = cylinders->GetFrequency();
    _parameters.push_back(frequency);
}

CylindersModule::~CylindersModule() {
}

void CylindersModule::UpdateParameters() {
    // Put parameters within bounds to avoid crash.

    // Apply parameters to module.
    noise::module::Cylinders* cylinders = static_cast< noise::module::Cylinders* >(_module);
    cylinders->SetFrequency(_parameters[0].value.decimal);
}
