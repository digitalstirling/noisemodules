#ifndef BILLOW_MODULE_HPP
#define BILLOW_MODULE_HPP

/**
 * @file BillowModule.hpp
 *
 * This module declares the BillowModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

/**
 * @todo Needs documentation
 */
class BillowModule
    : public Module
{
    // Public methods
public:
    /**
     * This is the instance constructor.
     */
    BillowModule();

    /**
     * This is the instance destructor.
     */
    ~BillowModule();

    // Module
protected:
    void UpdateParameters();
};

#endif /* BILLOW_MODULE_HPP */
