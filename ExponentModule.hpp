#ifndef EXPONENT_MODULE_HPP
#define EXPONENT_MODULE_HPP

/**
 * @file ExponentModule.hpp
 *
 * This module declares the ExponentModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

/**
 * @todo Needs documentation
 */
class ExponentModule
    : public Module
{
    // Public methods
public:
    /**
     * This is the instance constructor.
     */
    ExponentModule();

    /**
     * This is the instance destructor.
     */
    ~ExponentModule();

    // Module
protected:
    void UpdateParameters();
};

#endif /* EXPONENT_MODULE_HPP */
