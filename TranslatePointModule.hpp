#ifndef TRANSLATE_POINT_MODULE_HPP
#define TRANSLATE_POINT_MODULE_HPP

/**
 * @file TranslatePointModule.hpp
 *
 * This module declares the TranslatePointModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

/**
 * @todo Needs documentation
 */
class TranslatePointModule
    : public Module
{
    // Public methods
public:
    /**
     * This is the instance constructor.
     */
    TranslatePointModule();

    /**
     * This is the instance destructor.
     */
    ~TranslatePointModule();

    // Module
protected:
    void UpdateParameters();
};

#endif /* TRANSLATE_POINT_MODULE_HPP */
