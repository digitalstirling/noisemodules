#ifndef MODULES_HPP
#define MODULES_HPP

/**
 * @file Modules.hpp
 *
 * This module declares the Modules class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

#include <expat.h>
#include <map>
#include <noise.h>
#include <string>
#include <vector>

/**
 * This class represents a set of modules to be deserialized from an XML file.
 */
class Modules {
    // Custom types
public:
    /**
     * @todo Needs documentation
     */
    typedef std::vector< int > Sources;

    /**
     * This type is used to specify a method to call to handle the next
     * start element during the parsing of XML.
     */
    typedef void (Modules::* XmlStartElement)(const XML_Char* name, const XML_Char** atts);

    /**
     * This type is used to specify a method to call to handle the next
     * end element during the parsing of XML.
     */
    typedef void (Modules::* XmlEndElement)(const XML_Char* name);

    /**
     * @todo Needs documentation
     */
    struct SerializedModule {
        int id;
        Module* module;
        Sources sources;
    };

    /**
     * @todo Needs documentation
     */
    typedef std::map< int, SerializedModule > SerializedModules;

    // Public methods
public:
    /**
     * @todo Needs documentation
     */
    void BeginLoadingModules(XML_Parser xmlParser, class IModuleOwner* owner, int seedOffset);

    // Private methods
private:
    /**
     * This method is registered with Expat to be called whenever the
     * start of an element is parsed.
     *
     * @param[in] userData
     *     This is the pointer given to Expat to be passed to each callback.
     *     It will be the pointing to the App instance.
     *
     * @param[in] name
     *     This is the name of the element parsed.
     *
     * @param[in] atts
     *     This is a NULL-terminated array of pointers to the element's
     *     attributes.  The pointers alternate between attribute name and
     *     corresponding attribute value.
     */
    static void XMLCALL XmlStartElementHandler(void* userData, const XML_Char* name, const XML_Char** atts);

    /**
     * This method is registered with Expat to be called whenever the
     * end of an element is parsed.
     *
     * @param[in] userData
     *     This is the pointer given to Expat to be passed to each callback.
     *     It will be the pointing to the App instance.
     *
     * @param[in] name
     *     This is the name of the element parsed.
     */
    static void XMLCALL XmlEndElementHandler(void* userData, const XML_Char* name);

    /**
     * This method is registered with Expat to be called whenever character
     * data is parsed.
     *
     * @param[in] userData
     *     This is the pointer given to Expat to be passed to each callback.
     *     It will be the pointing to the App instance.
     *
     * @param[in] s
     *     This points to the character data parsed.
     *
     * @param[in] len
     *     This is the number of characters parsed.
     */
    static void XMLCALL XmlCharacterDataHandler(void* userData, const XML_Char* s, int len);

    /**
     * This method is called to store character data parsed from XML input.
     *
     * @param[in] s
     *     This points to the character data parsed.
     *
     * @param[in] len
     *     This is the number of characters parsed.
     */
    void XmlCharacterData(const XML_Char* s, int len);

    /**
     * This method is called whenever the
     * start of an element under the Modules element is parsed.
     *
     * @param[in] name
     *     This is the name of the element parsed.
     *
     * @param[in] atts
     *     This is a NULL-terminated array of pointers to the element's
     *     attributes.  The pointers alternate between attribute name and
     *     corresponding attribute value.
     */
    void XmlStartElementHandlerModules(const XML_Char* name, const XML_Char** atts);

    /**
     * This method is called whenever the
     * end of an element under a Modules element is parsed.
     *
     * @param[in] name
     *     This is the name of the element parsed.
     */
    void XmlEndElementHandlerModules(const XML_Char* name);

    /**
     * This method is called whenever the
     * start of an element under the Module element is parsed.
     *
     * @param[in] name
     *     This is the name of the element parsed.
     *
     * @param[in] atts
     *     This is a NULL-terminated array of pointers to the element's
     *     attributes.  The pointers alternate between attribute name and
     *     corresponding attribute value.
     */
    void XmlStartElementHandlerModule(const XML_Char* name, const XML_Char** atts);

    /**
     * This method is called whenever the
     * end of an element under a Module element is parsed.
     *
     * @param[in] userData
     *     This is the pointer given to Expat to be passed to each callback.
     *     It will be the pointing to the App instance.
     *
     * @param[in] name
     *     This is the name of the element parsed.
     */
    void XmlEndElementHandlerModule(const XML_Char* name);

    // Private properties
private:
    /**
     * @todo Needs documentation
     */
    int _seedOffset = 0;
    
    /**
     * @todo Needs documentation
     */
    class IModuleOwner* _owner = nullptr;

    /**
     * @todo Needs documentation
     */
    XML_Parser _xmlParser = NULL;

    /**
     * @todo Needs documentation
     */
    int _xmlSkip = 0;

    /**
     * @todo Needs documentation
     */
    bool _ownerElement = false;

    /**
     * @todo Needs documentation
     */
    SerializedModules _serializedModules;

    /**
     * @todo Needs documentation
     */
    SerializedModule _currentSerializedModule;

    /**
     * This points to the current method to call to handle the start
     * of an element while parsing XML input.
     */
    XmlStartElement _xmlStartElement;

    /**
     * This points to the current method to call to handle the end
     * of an element while parsing XML input.
     */
    XmlEndElement _xmlEndElement;
};

#endif /* MODULES_HPP */
