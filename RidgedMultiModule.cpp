/**
 * @file RidgedMultiModule.cpp
 *
 * This module contains the implementation of the RidgedMultiModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "RidgedMultiModule.hpp"

#include <algorithm>
#include <noise.h>

RidgedMultiModule::RidgedMultiModule()
    : Module("RidgedMulti", new noise::module::RidgedMulti())
{
    noise::module::RidgedMulti* ridgedMulti = static_cast< noise::module::RidgedMulti* >(_module);

    // Seed
    Parameter seed = { "Seed", ParameterTypeInteger };
    seed.value.integer = ridgedMulti->GetSeed();
    _parameters.push_back(seed);

    // Frequency
    Parameter frequency = { "Frequency", ParameterTypeDecimal };
    frequency.value.decimal = ridgedMulti->GetFrequency();
    _parameters.push_back(frequency);

    // Lacunarity
    Parameter lacunariy = { "Lacunarity", ParameterTypeDecimal };
    lacunariy.value.decimal = ridgedMulti->GetLacunarity();
    _parameters.push_back(lacunariy);

    // Quality
    Parameter quality = { "Quality", ParameterTypeQuality };
    quality.value.quality = ridgedMulti->GetNoiseQuality();
    _parameters.push_back(quality);

    // Octaves
    Parameter octaves = { "Octaves", ParameterTypeInteger };
    octaves.value.integer = ridgedMulti->GetOctaveCount();
    _parameters.push_back(octaves);
}

RidgedMultiModule::~RidgedMultiModule() {
}

void RidgedMultiModule::UpdateParameters() {
    // Put parameters within bounds to avoid crash.
    _parameters[4].value.integer = std::max(1, std::min(noise::module::RIDGED_MAX_OCTAVE, _parameters[4].value.integer));

    // Apply parameters to module.
    noise::module::RidgedMulti* ridgedMulti = static_cast< noise::module::RidgedMulti* >(_module);
    ridgedMulti->SetSeed(_parameters[0].value.integer);
    ridgedMulti->SetFrequency(_parameters[1].value.decimal);
    ridgedMulti->SetLacunarity(_parameters[2].value.decimal);
    ridgedMulti->SetNoiseQuality(_parameters[3].value.quality);
    ridgedMulti->SetOctaveCount(_parameters[4].value.integer);
}
