#ifndef MODULE_HPP
#define MODULE_HPP

/**
 * @file Module.hpp
 *
 * This module declares the Module class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include <expat.h>
#include <map>
#include <noise.h>
#include <string>
#include <vector>

/**
 * This class represents a module in the user interface.
 */
class Module {
    // Custom types
public:
    /**
     * @todo Needs documentation
     */
    enum ParameterType {
        ParameterTypeBoolean,
        ParameterTypeInteger,
        ParameterTypeDecimal,
        ParameterTypeQuality,
        ParameterTypeControlPointPairs,
        ParameterTypeControlPointSingles,
    };

    /**
     * @todo Needs documentation
     */
    union ParameterValue {
        bool boolean;                            // ParameterTypeBoolean
        int integer;                             // ParameterTypeInteger
        double decimal;                          // ParameterTypeDecimal
        noise::NoiseQuality quality;             // ParameterTypeQuality
        struct ControlPoint* controlPointPairs;  // ParameterTypeControlPointPairs
        double* controlPointSingles;             // ParameterTypeControlPointSingles
    };

    /**
     * @todo Needs documentation
     */
    struct Parameter {
        std::string name;
        ParameterType type;
        ParameterValue value;
        size_t valueLength;
    };

    /**
     * @todo Needs documentation
     */
    typedef std::vector< Parameter > Parameters;

    // Public methods
public:
    /**
     * This is the instance constructor.
     *
     * @param[in] label
     *     This is the text label to associate the module, indicating to
     *     the user the meaning, function, or purpose of the module.
     *
     * @param[in] module
     *     This is the implementation of the noise function in the module.
     */
    Module(std::string label, noise::module::Module* module);

    /**
     * This is the instance destructor.
     */
    virtual ~Module();

    /**
     * This method returns the number of sources for the module.
     *
     * @return
     *     The number of sources for the module is returned.
     */
    int GetNumSources() const;

    /**
     * @todo Needs documentation
     */
    double GetValue(double x, double y, double z);

    /**
     * @todo Needs documentation
     */
    Module* GetSource(int sourceIndex);

    /**
     * @todo Needs documentation
     */
    Module* SetSource(int sourceIndex, Module* source);

    /**
     * @todo Needs documentation
     */
    bool IsInSourceHierarchy(Module* module) const;

    /**
     * @todo Needs documentation
     */
    void SetName(std::string name);

    /**
     * @todo Needs documentation
     */
    std::string GetName() const;

    /**
     * @todo Needs documentation
     */
    Parameters& GetParameters();

    /**
     * @todo Needs documentation
     */
    virtual void UpdateParameters();

    /**
     * @todo Needs documentation
     */
    std::string GetLabel() const;

    // Protected properties
protected:
    /**
     * @todo Needs documentation
     */
    noise::module::Module* _module;

    /**
     * @todo Needs documentation
     */
    Parameters _parameters;

    // Private properties
private:
    /**
     * This is the text label to associate the module, indicating to
     * the user the meaning, function, or purpose of the module.
     */
    std::string _label;

    /**
     * @todo Needs documentation
     */
    std::string _name;

    /**
     * @todo Needs documentation
     */
    std::vector< Module* > _sources;
};

#endif /* MODULE_HPP */
