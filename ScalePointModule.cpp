/**
 * @file ScalePointModule.cpp
 *
 * This module contains the implementation of the ScalePointModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "ScalePointModule.hpp"

#include <algorithm>
#include <noise.h>

ScalePointModule::ScalePointModule()
    : Module("ScalePoint", new noise::module::ScalePoint())
{
    noise::module::ScalePoint* scalePoint = static_cast< noise::module::ScalePoint* >(_module);

    // X Scale
    Parameter xScale = { "X Scale", ParameterTypeDecimal };
    xScale.value.decimal = scalePoint->GetXScale();
    _parameters.push_back(xScale);

    // Y Scale
    Parameter yScale = { "Y Scale", ParameterTypeDecimal };
    yScale.value.decimal = scalePoint->GetYScale();
    _parameters.push_back(yScale);

    // Z Scale
    Parameter zScale = { "Z Scale", ParameterTypeDecimal };
    zScale.value.decimal = scalePoint->GetZScale();
    _parameters.push_back(zScale);
}

ScalePointModule::~ScalePointModule() {
}

void ScalePointModule::UpdateParameters() {
    // Put parameters within bounds to avoid crash.

    // Apply parameters to module.
    noise::module::ScalePoint* scalePoint = static_cast< noise::module::ScalePoint* >(_module);
    scalePoint->SetXScale(_parameters[0].value.decimal);
    scalePoint->SetYScale(_parameters[1].value.decimal);
    scalePoint->SetZScale(_parameters[2].value.decimal);
}
