/**
 * @file Modules.cpp
 *
 * This module contains the implementation of the Modules class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "BillowModule.hpp"
#include "ClampModule.hpp"
#include "ConstModule.hpp"
#include "CylindersModule.hpp"
#include "ExponentModule.hpp"
#include "Gradient.hpp"
#include "IModuleOwner.hpp"
#include "Modules.hpp"
#include "PerlinModule.hpp"
#include "RidgedMultiModule.hpp"
#include "RotatePointModule.hpp"
#include "ScaleBiasModule.hpp"
#include "ScalePointModule.hpp"
#include "SelectModule.hpp"
#include "SpheresModule.hpp"
#include "TranslatePointModule.hpp"
#include "TurbulenceModule.hpp"
#include "VoronoiModule.hpp"

#include <noise.h>
#include <stddef.h>
#include <string>

void Modules::BeginLoadingModules(XML_Parser xmlParser, IModuleOwner* owner, int seedOffset) {
    _xmlParser = xmlParser;
    _owner = owner;
    _seedOffset = seedOffset;
    XML_SetUserData(_xmlParser, this);
    XML_SetElementHandler(xmlParser, XmlStartElementHandler, XmlEndElementHandler);
    XML_SetCharacterDataHandler(xmlParser, XmlCharacterDataHandler);
    _xmlStartElement = &Modules::XmlStartElementHandlerModules;
    _xmlEndElement = &Modules::XmlEndElementHandlerModules;
    _xmlSkip = 0;
    _serializedModules.clear();
}

void XMLCALL Modules::XmlStartElementHandler(void* userData, const XML_Char* name, const XML_Char** atts) {
    Modules* self = reinterpret_cast< Modules* >(userData);
    if (self->_xmlSkip > 0) {
        ++self->_xmlSkip;
    } else if (self->_xmlSkip == 0) {
        (self->*self->_xmlStartElement)(name, atts);
    }
}

void XMLCALL Modules::XmlEndElementHandler(void* userData, const XML_Char* name) {
    Modules* self = reinterpret_cast< Modules* >(userData);
    if (self->_xmlSkip > 0) {
        --self->_xmlSkip;
    } else if (self->_xmlSkip == 0) {
        (self->*self->_xmlEndElement)(name);
    }
}

void XMLCALL Modules::XmlCharacterDataHandler(void* userData, const XML_Char* s, int len) {
    Modules* self = reinterpret_cast< Modules* >(userData);
    if (self->_xmlSkip == 0) {
        self->XmlCharacterData(s, len);
    }
}

void Modules::XmlCharacterData(const XML_Char* s, int len) {
    if (_ownerElement) {
        _owner->ModuleElementCharacterData(_currentSerializedModule.module, s, len);
    }
}

void Modules::XmlStartElementHandlerModules(const XML_Char* name, const XML_Char** atts) {
    if (_xmlSkip > 0) {
        ++_xmlSkip;
    } else if (_xmlSkip == 0) {
        if (name == std::string("Module")) {
            std::string type;
            bool idFound = false;
            int seed = noise::module::DEFAULT_PERLIN_SEED;
            double frequency = noise::module::DEFAULT_PERLIN_FREQUENCY;
            double lacunarity = noise::module::DEFAULT_PERLIN_LACUNARITY;
            int octaves = noise::module::DEFAULT_PERLIN_OCTAVE_COUNT;
            double persistence = noise::module::DEFAULT_PERLIN_PERSISTENCE;
            double lowerBound = noise::module::DEFAULT_CLAMP_LOWER_BOUND;
            double upperBound = noise::module::DEFAULT_CLAMP_UPPER_BOUND;
            double moduleValue = noise::module::DEFAULT_CONST_VALUE;
            double exponent = noise::module::DEFAULT_EXPONENT;
            double xAngle = noise::module::DEFAULT_ROTATE_X;
            double yAngle = noise::module::DEFAULT_ROTATE_Y;
            double zAngle = noise::module::DEFAULT_ROTATE_Z;
            double xScale = noise::module::DEFAULT_SCALE_POINT_X;
            double yScale = noise::module::DEFAULT_SCALE_POINT_Y;
            double zScale = noise::module::DEFAULT_SCALE_POINT_Z;
            double xTranslation = noise::module::DEFAULT_TRANSLATE_POINT_X;
            double yTranslation = noise::module::DEFAULT_TRANSLATE_POINT_Y;
            double zTranslation = noise::module::DEFAULT_TRANSLATE_POINT_Z;
            double bias = noise::module::DEFAULT_BIAS;
            double scale = noise::module::DEFAULT_SCALE;
            double edgeFalloff = noise::module::DEFAULT_SELECT_EDGE_FALLOFF;
            double power = noise::module::DEFAULT_TURBULENCE_POWER;
            int roughnessCount = noise::module::DEFAULT_TURBULENCE_ROUGHNESS;
            const XML_Char** originalAttributes = atts;
            std::string moduleName;
            while (*atts != NULL) {
                const XML_Char* name = *atts++;
                const XML_Char* value = *atts++;
                if (name == std::string("Type")) {
                    type = value;
                } else if (name == std::string("Name")) {
                    moduleName = value;
                } else if (name == std::string("Id")) {
                    int valueInt;
                    if (sscanf(value, "%d", &valueInt) == 1) {
                        _currentSerializedModule.id = valueInt;
                        idFound = true;
                    }
                } else if (name == std::string("Bias")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        bias = valueFloat;
                    }
                } else if (name == std::string("Scale")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        scale = valueFloat;
                    }
                } else if (name == std::string("Power")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        power = valueFloat;
                    }
                } else if (name == std::string("Edge_Falloff")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        edgeFalloff = valueFloat;
                    }
                } else if (name == std::string("Seed")) {
                    int valueInt;
                    if (sscanf(value, "%d", &valueInt) == 1) {
                        seed = valueInt;
                    }
                } else if (name == std::string("Exponent")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        exponent = valueFloat;
                    }
                } else if (name == std::string("Frequency")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        frequency = valueFloat;
                    }
                } else if (name == std::string("Lacunarity")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        lacunarity = valueFloat;
                    }
                } else if (name == std::string("Lower_Bound")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        lowerBound = valueFloat;
                    }
                } else if (name == std::string("Octaves")) {
                    int valueInt;
                    if (sscanf(value, "%d", &valueInt) == 1) {
                        octaves = valueInt;
                    }
                } else if (name == std::string("Persistence")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        persistence = valueFloat;
                    }
                } else if (name == std::string("Upper_Bound")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        upperBound = valueFloat;
                    }
                } else if (name == std::string("Value")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        moduleValue = valueFloat;
                    }
                } else if (name == std::string("X_Angle")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        xAngle = valueFloat;
                    }
                } else if (name == std::string("Y_Angle")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        yAngle = valueFloat;
                    }
                } else if (name == std::string("Z_Angle")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        zAngle = valueFloat;
                    }
                } else if (name == std::string("X_Scale")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        xScale = valueFloat;
                    }
                } else if (name == std::string("Y_Scale")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        yScale = valueFloat;
                    }
                } else if (name == std::string("Z_Scale")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        zScale = valueFloat;
                    }
                } else if (name == std::string("X_Translation")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        xTranslation = valueFloat;
                    }
                } else if (name == std::string("Y_Translation")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        yTranslation = valueFloat;
                    }
                } else if (name == std::string("Z_Translation")) {
                    double valueFloat;
                    if (sscanf(value, "%lf", &valueFloat) == 1) {
                        zTranslation = valueFloat;
                    }
                } else if (name == std::string("Roughness_Count")) {
                    int valueInt;
                    if (sscanf(value, "%d", &valueInt) == 1) {
                        roughnessCount = valueInt;
                    }
                }
            }
            Module* module = NULL;
            if (idFound) {
                if (type == "Abs") {
                    module = new Module("Abs", new noise::module::Abs());
                } else if (type == "Add") {
                    module = new Module("Add", new noise::module::Add());
                } else if (type == "Billow") {
                    module = new BillowModule();
                } else if (type == "Blend") {
                    module = new Module("Blend", new noise::module::Blend());
                } else if (type == "Cache") {
                    module = new Module("Cache", new noise::module::Cache());
                } else if (type == "Checkerboard") {
                    module = new Module("Checkerboard", new noise::module::Checkerboard());
                } else if (type == "Clamp") {
                    module = new ClampModule();
                } else if (type == "Const") {
                    module = new ConstModule();
                } else if (type == "Curve") {
                    module = new Module("Curve", new noise::module::Curve());
                } else if (type == "Cylinders") {
                    module = new CylindersModule();
                } else if (type == "Displace") {
                    module = new Module("Displace", new noise::module::Displace());
                } else if (type == "Exponent") {
                    module = new ExponentModule();
                } else if (type == "Invert") {
                    module = new Module("Invert", new noise::module::Invert());
                } else if (type == "Max") {
                    module = new Module("Max", new noise::module::Max());
                } else if (type == "Min") {
                    module = new Module("Min", new noise::module::Min());
                } else if (type == "Multiply") {
                    module = new Module("Multiply", new noise::module::Multiply());
                } else if (type == "Perlin") {
                    module = new PerlinModule();
                } else if (type == "Power") {
                    module = new Module("Power", new noise::module::Power());
                } else if (type == "RidgedMulti") {
                    module = new RidgedMultiModule();
                } else if (type == "RotatePoint") {
                    module = new RotatePointModule();
                } else if (type == "ScaleBias") {
                    module = new ScaleBiasModule();
                } else if (type == "ScalePoint") {
                    module = new ScalePointModule();
                } else if (type == "Select") {
                    module = new SelectModule();
                } else if (type == "Spheres") {
                    module = new SpheresModule();
                } else if (type == "Terrace") {
                    module = new Module("Terrace", new noise::module::Terrace());
                } else if (type == "TranslatePoint") {
                    module = new TranslatePointModule();
                } else if (type == "Turbulence") {
                    module = new TurbulenceModule();
                } else if (type == "Voronoi") {
                    module = new VoronoiModule();
                } else if (type == "Gradient") {
                    module = new Module("Gradient", new Gradient());
                }
            }
            if (module != NULL) {
                module->SetName(moduleName);
                Module::Parameters& parameters = module->GetParameters();
                for (Module::Parameters::iterator it = parameters.begin(), end = parameters.end(); it != end; ++it) {
                    if (it->name == "Seed") {
                        it->value.integer = seed + _seedOffset;
                    } else if (it->name == "Bias") {
                        it->value.decimal = bias;
                    } else if (it->name == "Scale") {
                        it->value.decimal = scale;
                    } else if (it->name == "Frequency") {
                        it->value.decimal = frequency;
                    } else if (it->name == "Lacunarity") {
                        it->value.decimal = lacunarity;
                    } else if (it->name == "Exponent") {
                        it->value.decimal = exponent;
                    } else if (it->name == "Edge Falloff") {
                        it->value.decimal = edgeFalloff;
                    } else if (it->name == "Lower Bound") {
                        it->value.decimal = lowerBound;
                    } else if (it->name == "Octaves") {
                        it->value.integer = octaves;
                    } else if (it->name == "Persistence") {
                        it->value.decimal = persistence;
                    } else if (it->name == "Power") {
                        it->value.decimal = power;
                    } else if (it->name == "Upper Bound") {
                        it->value.decimal = upperBound;
                    } else if (it->name == "Value") {
                        it->value.decimal = moduleValue;
                    } else if (it->name == "X Angle") {
                        it->value.decimal = xAngle;
                    } else if (it->name == "Y Angle") {
                        it->value.decimal = yAngle;
                    } else if (it->name == "Z Angle") {
                        it->value.decimal = zAngle;
                    } else if (it->name == "X Scale") {
                        it->value.decimal = xScale;
                    } else if (it->name == "Y Scale") {
                        it->value.decimal = yScale;
                    } else if (it->name == "Z Scale") {
                        it->value.decimal = zScale;
                    } else if (it->name == "X Translation") {
                        it->value.decimal = xTranslation;
                    } else if (it->name == "Y Translation") {
                        it->value.decimal = yTranslation;
                    } else if (it->name == "Z Translation") {
                        it->value.decimal = zTranslation;
                    } else if (it->name == "Roughness Count") {
                        it->value.integer = roughnessCount;
                    }
                }
                module->UpdateParameters();
                _currentSerializedModule.sources.clear();
                _currentSerializedModule.module = module;
                _owner->BeginLoadingModule(module, _currentSerializedModule.id, originalAttributes);
                _xmlStartElement = &Modules::XmlStartElementHandlerModule;
                _xmlEndElement = &Modules::XmlEndElementHandlerModule;
            } else {
                _xmlSkip = 1;
            }
        } else {
            _xmlSkip = 1;
        }
    }
}

void Modules::XmlEndElementHandlerModules(const XML_Char* name) {
    if (_xmlSkip > 0) {
        --_xmlSkip;
    } else if (_xmlSkip == 0) {
        for (SerializedModules::iterator it = _serializedModules.begin(), end = _serializedModules.end(); it != end; ++it) {
            for (int port = 0; port < (int)it->second.sources.size(); ++port) {
                SerializedModules::iterator source = _serializedModules.find(it->second.sources[port]);
                if (source != _serializedModules.end()) {
                    it->second.module->SetSource(port, source->second.module);
                }
            }
        }
        _owner->EndLoadingModules(_serializedModules);
        _serializedModules.clear();
    }
}

void Modules::XmlStartElementHandlerModule(const XML_Char* name, const XML_Char** atts) {
    if (_ownerElement) {
        _owner->StartModuleElement(_currentSerializedModule.module, name, atts);
    }
    if (_xmlSkip > 0) {
        ++_xmlSkip;
    } else if (_xmlSkip == 0) {
        _ownerElement = false;
        if (name == std::string("Source")) {
            int id = 0;
            bool idFound = false;
            while (*atts != NULL) {
                const XML_Char* name = *atts++;
                const XML_Char* value = *atts++;
                if (name == std::string("Id")) {
                    int valueInt;
                    if (sscanf(value, "%d", &valueInt) == 1) {
                        id = valueInt;
                        idFound = true;
                    }
                }
            }
            if (idFound) {
                _currentSerializedModule.sources.push_back(id);
            }
        } else {
            _ownerElement = true;
            _owner->StartModuleElement(_currentSerializedModule.module, name, atts);
        }
        _xmlSkip = 1;
    }
}

void Modules::XmlEndElementHandlerModule(const XML_Char* name) {
    if (_ownerElement) {
        _owner->EndModuleElement(_currentSerializedModule.module, name);
    }
    if (_xmlSkip > 0) {
        --_xmlSkip;
    } else if (_xmlSkip == 0) {
        _xmlStartElement = &Modules::XmlStartElementHandlerModules;
        _xmlEndElement = &Modules::XmlEndElementHandlerModules;
        _serializedModules[_currentSerializedModule.id] = _currentSerializedModule;
    }
}
