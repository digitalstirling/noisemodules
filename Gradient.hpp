#ifndef GRADIENT_HPP
#define GRADIENT_HPP

/**
 * @file Gradient.hpp
 *
 * This module declares the Gradient class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include <module/modulebase.h>

/// Default constant value for the noise::module::Const noise module.
const double DEFAULT_CONST_VALUE = 0.0;

/// Noise module that outputs a constant value.
///
/// @image html moduleconst.png
///
/// To specify the constant value, call the SetConstValue() method.
///
/// This noise module is not useful by itself, but it is often used as a
/// source module for other noise modules.
///
/// This noise module does not require any source modules.
class Gradient
    : public noise::module::Module
{
    // Public methods
public:
    /**
     * @todo Needs documentation
     */
    Gradient();

    /**
     * @todo Needs documentation
     */
    virtual int GetSourceModuleCount() const {
        return 0;
    }

    /**
     * @todo Needs documentation
     */
    virtual double GetValue(double x, double y, double z) const {
        return x + y + z;
    }
};

#endif /* GRADIENT_HPP */
