/**
 * @file ScaleBiasModule.cpp
 *
 * This module contains the implementation of the ScaleBiasModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "ScaleBiasModule.hpp"

#include <algorithm>
#include <noise.h>

ScaleBiasModule::ScaleBiasModule()
    : Module("ScaleBias", new noise::module::ScaleBias())
{
    noise::module::ScaleBias* scaleBias = static_cast< noise::module::ScaleBias* >(_module);

    // Bias
    Parameter bias = { "Bias", ParameterTypeDecimal };
    bias.value.decimal = scaleBias->GetBias();
    _parameters.push_back(bias);

    // Scale
    Parameter scale = { "Scale", ParameterTypeDecimal };
    scale.value.decimal = scaleBias->GetScale();
    _parameters.push_back(scale);
}

ScaleBiasModule::~ScaleBiasModule() {
}

void ScaleBiasModule::UpdateParameters() {
    // Put parameters within bounds to avoid crash.

    // Apply parameters to module.
    noise::module::ScaleBias* scaleBias = static_cast< noise::module::ScaleBias* >(_module);
    scaleBias->SetBias(_parameters[0].value.decimal);
    scaleBias->SetScale(_parameters[1].value.decimal);
}
