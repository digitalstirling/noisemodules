#ifndef SPHERES_MODULE_HPP
#define SPHERES_MODULE_HPP

/**
 * @file SpheresModule.hpp
 *
 * This module declares the SpheresModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

/**
 * @todo Needs documentation
 */
class SpheresModule
    : public Module
{
    // Public methods
public:
    /**
     * This is the instance constructor.
     */
    SpheresModule();

    /**
     * This is the instance destructor.
     */
    ~SpheresModule();

    // Module
protected:
    void UpdateParameters();
};

#endif /* SPHERES_MODULE_HPP */
