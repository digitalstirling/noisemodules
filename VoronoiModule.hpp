#ifndef VORONOI_MODULE_HPP
#define VORONOI_MODULE_HPP

/**
 * @file VoronoiModule.hpp
 *
 * This module declares the VoronoiModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

/**
 * @todo Needs documentation
 */
class VoronoiModule
    : public Module
{
    // Public methods
public:
    /**
     * This is the instance constructor.
     */
    VoronoiModule();

    /**
     * This is the instance destructor.
     */
    ~VoronoiModule();

    // Module
protected:
    void UpdateParameters();
};

#endif /* VORONOI_MODULE_HPP */
