/**
 * @file BillowModule.cpp
 *
 * This module contains the implementation of the BillowModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "BillowModule.hpp"

#include <algorithm>
#include <noise.h>

BillowModule::BillowModule()
    : Module("Billow", new noise::module::Billow())
{
    noise::module::Billow* billow = static_cast< noise::module::Billow* >(_module);

    // Seed
    Parameter seed = { "Seed", ParameterTypeInteger };
    seed.value.integer = billow->GetSeed();
    _parameters.push_back(seed);

    // Frequency
    Parameter frequency = { "Frequency", ParameterTypeDecimal };
    frequency.value.decimal = billow->GetFrequency();
    _parameters.push_back(frequency);

    // Lacunarity
    Parameter lacunariy = { "Lacunarity", ParameterTypeDecimal };
    lacunariy.value.decimal = billow->GetLacunarity();
    _parameters.push_back(lacunariy);

    // Quality
    Parameter quality = { "Quality", ParameterTypeQuality };
    quality.value.quality = billow->GetNoiseQuality();
    _parameters.push_back(quality);

    // Octaves
    Parameter octaves = { "Octaves", ParameterTypeInteger };
    octaves.value.integer = billow->GetOctaveCount();
    _parameters.push_back(octaves);

    // Persistence
    Parameter persistence = { "Persistence", ParameterTypeDecimal };
    persistence.value.decimal = billow->GetPersistence();
    _parameters.push_back(persistence);
}

BillowModule::~BillowModule() {
}

void BillowModule::UpdateParameters() {
    // Put parameters within bounds to avoid crash.
    _parameters[4].value.integer = std::max(1, std::min(noise::module::BILLOW_MAX_OCTAVE, _parameters[4].value.integer));

    // Apply parameters to module.
    noise::module::Billow* billow = static_cast< noise::module::Billow* >(_module);
    billow->SetSeed(_parameters[0].value.integer);
    billow->SetFrequency(_parameters[1].value.decimal);
    billow->SetLacunarity(_parameters[2].value.decimal);
    billow->SetNoiseQuality(_parameters[3].value.quality);
    billow->SetOctaveCount(_parameters[4].value.integer);
    billow->SetPersistence(_parameters[5].value.decimal);
}
