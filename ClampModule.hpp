#ifndef CLAMP_MODULE_HPP
#define CLAMP_MODULE_HPP

/**
 * @file ClampModule.hpp
 *
 * This module declares the ClampModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

/**
 * @todo Needs documentation
 */
class ClampModule
    : public Module
{
    // Public methods
public:
    /**
     * This is the instance constructor.
     */
    ClampModule();

    /**
     * This is the instance destructor.
     */
    ~ClampModule();

    // Module
protected:
    void UpdateParameters();
};

#endif /* CLAMP_MODULE_HPP */
