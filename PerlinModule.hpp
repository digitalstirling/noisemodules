#ifndef PERLIN_MODULE_HPP
#define PERLIN_MODULE_HPP

/**
 * @file PerlinModule.hpp
 *
 * This module declares the PerlinModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

/**
 * @todo Needs documentation
 */
class PerlinModule
    : public Module
{
    // Public methods
public:
    /**
     * This is the instance constructor.
     */
    PerlinModule();

    /**
     * This is the instance destructor.
     */
    ~PerlinModule();

    // Module
protected:
    void UpdateParameters();
};

#endif /* PERLIN_MODULE_HPP */
