/**
 * @file TurbulenceModule.cpp
 *
 * This module contains the implementation of the TurbulenceModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "TurbulenceModule.hpp"

#include <algorithm>
#include <noise.h>

TurbulenceModule::TurbulenceModule()
    : Module("Turbulence", new noise::module::Turbulence())
{
    noise::module::Turbulence* turbulence = static_cast< noise::module::Turbulence* >(_module);

    // Frequency
    Parameter frequency = { "Frequency", ParameterTypeDecimal };
    frequency.value.decimal = turbulence->GetFrequency();
    _parameters.push_back(frequency);

    // Power
    Parameter power = { "Power", ParameterTypeDecimal };
    power.value.decimal = turbulence->GetPower();
    _parameters.push_back(power);

    // Roughness Count
    Parameter roughnessCount = { "Roughness Count", ParameterTypeInteger };
    roughnessCount.value.integer = turbulence->GetRoughnessCount();
    _parameters.push_back(roughnessCount);

    // Seed
    Parameter seed = { "Seed", ParameterTypeInteger };
    seed.value.integer = turbulence->GetSeed();
    _parameters.push_back(seed);
}

TurbulenceModule::~TurbulenceModule() {
}

void TurbulenceModule::UpdateParameters() {
    // Put parameters within bounds to avoid crash.
    _parameters[2].value.integer = std::max(1, std::min(noise::module::PERLIN_MAX_OCTAVE, _parameters[2].value.integer));

    // Apply parameters to module.
    noise::module::Turbulence* turbulence = static_cast< noise::module::Turbulence* >(_module);
    turbulence->SetFrequency(_parameters[0].value.decimal);
    turbulence->SetPower(_parameters[1].value.decimal);
    turbulence->SetRoughness(_parameters[2].value.integer);
    turbulence->SetSeed(_parameters[3].value.integer);
}
