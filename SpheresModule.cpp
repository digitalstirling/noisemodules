/**
 * @file SpheresModule.cpp
 *
 * This module contains the implementation of the SpheresModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "SpheresModule.hpp"

#include <algorithm>
#include <noise.h>

SpheresModule::SpheresModule()
    : Module("Spheres", new noise::module::Spheres())
{
    noise::module::Spheres* spheres = static_cast< noise::module::Spheres* >(_module);

    // Frequency
    Parameter frequency = { "Frequency", ParameterTypeDecimal };
    frequency.value.decimal = spheres->GetFrequency();
    _parameters.push_back(frequency);
}

SpheresModule::~SpheresModule() {
}

void SpheresModule::UpdateParameters() {
    // Put parameters within bounds to avoid crash.

    // Apply parameters to module.
    noise::module::Spheres* spheres = static_cast< noise::module::Spheres* >(_module);
    spheres->SetFrequency(_parameters[0].value.decimal);
}
