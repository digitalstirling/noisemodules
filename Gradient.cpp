/**
 * @file Gradient.cpp
 *
 * This module contains the implementation of the Gradient class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Gradient.hpp"

Gradient::Gradient()
    : noise::module::Module(GetSourceModuleCount())
{
}
