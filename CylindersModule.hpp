#ifndef CYLINDERS_MODULE_HPP
#define CYLINDERS_MODULE_HPP

/**
 * @file CylindersModule.hpp
 *
 * This module declares the CylindersModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

/**
 * @todo Needs documentation
 */
class CylindersModule
    : public Module
{
    // Public methods
public:
    /**
     * This is the instance constructor.
     */
    CylindersModule();

    /**
     * This is the instance destructor.
     */
    ~CylindersModule();

    // Module
protected:
    void UpdateParameters();
};

#endif /* CYLINDERS_MODULE_HPP */
