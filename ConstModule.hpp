#ifndef CONST_MODULE_HPP
#define CONST_MODULE_HPP

/**
 * @file ConstModule.hpp
 *
 * This module declares the ConstModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

/**
 * @todo Needs documentation
 */
class ConstModule
    : public Module
{
    // Public methods
public:
    /**
     * This is the instance constructor.
     */
    ConstModule();

    /**
     * This is the instance destructor.
     */
    ~ConstModule();

    // Module
protected:
    void UpdateParameters();
};

#endif /* CONST_MODULE_HPP */
