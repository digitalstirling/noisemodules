/**
 * @file Module.cpp
 *
 * This module contains the implementation of the Module class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "BillowModule.hpp"
#include "ClampModule.hpp"
#include "ConstModule.hpp"
#include "CylindersModule.hpp"
#include "ExponentModule.hpp"
#include "Gradient.hpp"
#include "IModuleOwner.hpp"
#include "Module.hpp"
#include "PerlinModule.hpp"
#include "RidgedMultiModule.hpp"
#include "RotatePointModule.hpp"
#include "ScaleBiasModule.hpp"
#include "ScalePointModule.hpp"
#include "SelectModule.hpp"
#include "SpheresModule.hpp"
#include "TranslatePointModule.hpp"
#include "TurbulenceModule.hpp"
#include "VoronoiModule.hpp"

#include <noise.h>
#include <stddef.h>
#include <string>

Module::Module(std::string label, noise::module::Module* module)
    : _module(module)
    , _label(label)
    , _sources((module == NULL) ? 0 : module->GetSourceModuleCount())
{
}

Module::~Module() {
    delete _module;
}

int Module::GetNumSources() const {
    return (int)_sources.size();
}

double Module::GetValue(double x, double y, double z) {
    return (_module == NULL) ? 0 : _module->GetValue(x, y, z);
}

Module* Module::GetSource(int sourceIndex) {
    return _sources[sourceIndex];
}

Module* Module::SetSource(int sourceIndex, Module* source) {
    Module* oldSource = _sources[sourceIndex];
    _sources[sourceIndex] = source;
    if (source != NULL) {
        _module->SetSourceModule(sourceIndex, *source->_module);
    }
    return oldSource;
}

bool Module::IsInSourceHierarchy(Module* module) const {
    if (module == this) {
        return true;
    }
    for (auto source: _sources) {
        if (source == module) {
            return true;
        }
        if (source == nullptr) {
            continue;
        }
        if (source->IsInSourceHierarchy(module)) {
            return true;
        }
    }
    return false;
}

void Module::SetName(std::string name) {
    _name = name;
}

std::string Module::GetName() const {
    return _name;
}

Module::Parameters& Module::GetParameters() {
    return _parameters;
}

void Module::UpdateParameters() {
}

std::string Module::GetLabel() const {
    return _label;
}
