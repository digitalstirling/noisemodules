#ifndef ROTATE_POINT_MODULE_HPP
#define ROTATE_POINT_MODULE_HPP

/**
 * @file RotatePointModule.hpp
 *
 * This module declares the RotatePointModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "Module.hpp"

/**
 * @todo Needs documentation
 */
class RotatePointModule
    : public Module
{
    // Public methods
public:
    /**
     * This is the instance constructor.
     */
    RotatePointModule();

    /**
     * This is the instance destructor.
     */
    ~RotatePointModule();

    // Module
protected:
    void UpdateParameters();
};

#endif /* ROTATE_POINT_MODULE_HPP */
