/**
 * @file ClampModule.cpp
 *
 * This module contains the implementation of the ClampModule class.
 *
 * Copyright (c) 2014-2016 by Richard Walters
 */

#include "ClampModule.hpp"

#include <algorithm>
#include <noise.h>

ClampModule::ClampModule()
    : Module("Clamp", new noise::module::Clamp())
{
    noise::module::Clamp* clamp = static_cast< noise::module::Clamp* >(_module);

    // Lower Bound
    Parameter lowerBound = { "Lower Bound", ParameterTypeDecimal };
    lowerBound.value.decimal = clamp->GetLowerBound();
    _parameters.push_back(lowerBound);

    // Upper Bound
    Parameter upperBound = { "Upper Bound", ParameterTypeDecimal };
    upperBound.value.decimal = clamp->GetUpperBound();
    _parameters.push_back(upperBound);
}

ClampModule::~ClampModule() {
}

void ClampModule::UpdateParameters() {
    // Put parameters within bounds to avoid crash.

    // Apply parameters to module.
    noise::module::Clamp* clamp = static_cast< noise::module::Clamp* >(_module);
    clamp->SetBounds(_parameters[0].value.decimal, _parameters[1].value.decimal);
}
